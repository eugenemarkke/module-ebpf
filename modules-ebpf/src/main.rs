#![no_std]
#![no_main]

use aya_ebpf::{
    EbpfContext,
    maps::PerCpuArray,
    macros::{map,tracepoint},
    programs::TracePointContext,
    helpers::bpf_probe_read_kernel_str_bytes
};
use aya_log_ebpf::info;
use modules_common::Name;

#[map]
static mut NAME_BUFFER: PerCpuArray<Name> = PerCpuArray::with_max_entries(1,0);

#[tracepoint]
pub fn modules(ctx: TracePointContext) -> u32 {
    match try_modules(ctx) {
        Ok(ret) => ret,
        Err(ret) => ret as u32,
    }
}

fn try_modules(ctx: TracePointContext) -> Result<u32, i64> {
    let offset: u32 = unsafe {ctx.read_at::<u32>(8)? & 0xFFFF};
    let ptr = ctx.as_ptr() + offset as *mut _;
    let buf_ptr = unsafe { NAME_BUFFER.get_ptr_mut(0).ok_or(0)? };
    let buf = unsafe { &mut *buf_ptr };

    match unsafe { bpf_probe_read_kernel_str_bytes(ptr as *const u8, &mut buf.name) } {
        Ok(bytes) => info!(&ctx, "BYTES :)"),
        Err(err) => info!(&ctx, "NO BYTES :( {}", err)
    }

    info!(&ctx, "tracepoint module_free called");
    Ok(0)
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
