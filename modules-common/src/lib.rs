#![no_std]

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Name {
    pub name: [u8; 256]
}

#[cfg(feature = "userspace")]
mod userspace {
    use super::*;
    unsafe impl aya::Pod for Name {}
}
